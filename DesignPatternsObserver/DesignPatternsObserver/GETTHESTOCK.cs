using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace DesignPatternsObserver
{
    public class GetTheStock
    {

        //  Could be used to set how many seconds to wait
        //  in Thread.sleep() below
        //  private int startTime; 
        private String stock;
        double randNum;
        private double price;

        //  Will hold reference to the StockGrabber object
        private Subject stockGrabber;

        public GetTheStock(Subject stockGrabber, int newStartTime, String newStock, double newPrice)
        {
            //  Store the reference to the stockGrabber object so
            //  I can make calls to its methods
            this.stockGrabber = stockGrabber;
            //  startTime = newStartTime;  Not used to have variable sleep time
            this.stock = newStock;
            this.price = newPrice;
        }

        public void run()
        {
            for (int i = 1; (i <= 20); i++)
            {
                try
                {
                    //  Sleep for 2 seconds
                    Thread.Sleep(2000);
                    //  Use Thread.sleep(startTime * 1000); to 
                    //  make sleep time variable
                }
                catch (InvalidCastException e)
                {

                }

                //  Generates a random number between -.03 and .03
                

                //  Formats decimals to 2 places
                DecimalFormat df = new DecimalFormat("#.##");
                //  Change the price and then convert it back into a double
                this.price = Double.Parse(df.format(this.price + randNum));
                if ((this.stock == "IBM"))
                {
                    ((StockGrabber)(this.stockGrabber)).setIBMPrice(this.price);
                }

                if ((this.stock == "AAPL"))
                {
                    ((StockGrabber)(this.stockGrabber)).setAAPLPrice(this.price);
                }

                if ((this.stock == "GOOG"))
                {
                    ((StockGrabber)(this.stockGrabber)).setGOOGPrice(this.price);
                }

                Console.WriteLine((this.stock + (": "
                                + (df.format((this.price + randNum)) + (" " + df.format(randNum))))));
                Console.WriteLine();
            }

        }
    }
}